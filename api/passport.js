module.exports = function (passport, User) {

    const LocalStrategy = require('passport-local').Strategy;

    passport.isAuthenticated = function () {
        return function (req, res, next) {
            if (req.isAuthenticated()) {
                next();
            } else {
                res.status(401).send("User not logged")
            }
        }
    }

    passport.serializeUser((user, done) => {
        done(null, user.id);
    })

    passport.deserializeUser((id, done) => {
        User.findOne({ _id: id }, (err, user) => {
            done(err, user || null);
        });
    })

    passport.use(new LocalStrategy((username, password, done) => {
        User.findOne({ username: username, password: password }, (err, user) => {
            done(err, user || null);
        });
    }))
}