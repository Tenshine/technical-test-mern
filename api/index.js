var express = require('express');
var session = require('cookie-session')
var cookieParser = require('cookie-parser')
var bodyParser = require('body-parser')
const passport = require('passport')
var app = express()
const mongoose = require('mongoose');
const uri = 'mongodb://localhost/technical-test-mern';
const db = mongoose.connection;
const cors = require('cors');

mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true });

app.use(cors({
    origin: ['http://localhost:3000'],
    credentials: true,
    methods: 'GET, POST',
    allowedHeaders: ['Content-Type', 'Set-Cookie', '*']
}))

app.use(session({
    name: 'session',
    secret: 'passsecret',
    resave: false,
    saveUninitialized: false
}));

app.use(cookieParser())
app.use(bodyParser.json())
app.use(passport.initialize())
app.use(passport.session())

db.on('error', (err) => { console.log(err) });
db.once('open', function () {

    var UserSchema = new mongoose.Schema({
        username: { type: String, unique: true },
        password: String,
        favorites: [String]
    });

    const User = mongoose.model('User', UserSchema);

    require('./passport')(passport, User)

    app.get("/user", passport.isAuthenticated(), (req, res) => {
        User.findOne({ _id: req.user._id }, (err, user) => {
            res.send({ username: user.username })
        });
    })

    app.get("/favorites", passport.isAuthenticated(), (req, res) => {
        User.findOne({ _id: req.user._id }, (err, user) => {
            res.send(user.favorites)
        });
    })

    app.post("/favorites/add/:id", passport.isAuthenticated(), (req, res) => {
        User.updateOne({ _id: req.user._id }, { $addToSet: { favorites: req.params.id } }, (err, user) => {
            if (err)
                return res.status(400).send({ message: "Error" })
            res.send({ message: "Success" })
        });
    })

    app.post("/favorites/remove/:id", passport.isAuthenticated(), (req, res) => {
        User.updateOne({ _id: req.user._id }, { $pull: { favorites: req.params.id } }, (err, user) => {
            if (err)
                return res.status(400).send({ message: "Error" })
            res.send({ message: "Success" })
        });
    })

    app.post('/login', passport.authenticate('local'), (req, res) => {
        res.send({ message: "Success" });
    })

    app.post("/register", (req, res) => {
        if (!req.body.username || !req.body.password)
            return res.status(400).send({ message: "Error" })
        user = new User({ username: req.body.username, password: req.body.password });
        user.save(function (err, user) {
            if (err) {
                if (err.code == 11000)
                    return res.status(400).send({ message: "Username already taken" });
                return res.status(400).send({ message: "Error" });
            }
            res.send({ message: "Success" });
        })
    })

    console.log(`App listening on port 4000`)
    app.listen(4000)
});