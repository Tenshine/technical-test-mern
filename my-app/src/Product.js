import React from 'react';
import logo from './logo.svg';
import './App.css';
import ListGroup from 'react-bootstrap/ListGroup'
import Button from 'react-bootstrap/Button'
import { register } from './serviceWorker';

export default class Product extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      shoe: {
        media: {}
      }
    }
  }

  componentDidMount() {
    console.log(this.props.match.params.shoe)
    fetch("https://stockx.com/api/products/" + this.props.match.params.shoe + "?includes=market,360&currency=EUR&country=FR").then(res => {
      return res.json().then(data => {
        this.setState({
          shoe: data.Product
        })
        console.log(this.state)
      })
    })
  }

  render() {
    return (
      <div className="col=md-12">
        <div className="col=md-12">
          <h1>{this.state.shoe.title}</h1>
          <img src={this.state.shoe.media.imageUrl}></img>
        </div>
        <div className="row">
        <div className="col-md-6 offset-1" style={{outline: "auto"}}>
          <h5>Description</h5>
          <p>{this.state.shoe.description}</p>
        </div>
        <div className="col-md-3 offset-1" style={{outline: "auto"}}>
          <p>Date de sortie: {this.state.shoe.releaseDate}</p>
          <p>Marque: {this.state.shoe.brand}</p>
          <p>Sexe: {this.state.shoe.gender}</p>
          <p>Prix d'achat: {this.state.shoe.retailPrice}$</p>
          <p>Prix de revente: {this.state.shoe.minimumBid}$</p>
        </div>
        </div>
      </div>
    )
  }
}