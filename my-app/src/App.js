import React from 'react';
import logo from './logo.svg';
import './App.css';
import ListGroup from 'react-bootstrap/ListGroup'
import Button from 'react-bootstrap/Button'
import Product from './Product'
import Login from './Login'
import { register } from './serviceWorker';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink,
  Link
} from "react-router-dom";
import Favorite from '@material-ui/icons/Favorite';
import FavoriteBorder from '@material-ui/icons/FavoriteBorder';
import Checkbox from '@material-ui/core/Checkbox';

export default class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      username: null
    }
  }

  componentDidMount() {
    fetch("http://localhost:4000/user", { credentials: "include" }).then(res => {
      return res.json().then(data => {
        this.setState({
          username: data.username
        })
      })
    })
  }

  Log() {
    console.log(this.state.username)
    if (this.state.username)
      return (<p>Hello, {this.state.username}</p>)
  }

  render() {
    return (
      <Router>
        <div className="App">
          <header className="App-header">
            <div className="row" style={{ maxWidth: "100%" }}>
              <div className="col-md-1">
                <Link to='/'>Home</Link>
              </div>
              <div className="col-md-1">
                <Link to='/Login'>Login</Link>
              </div>
              <div className="col-md-4 offset-2">
                <h1 className="App-title">Sneaker City</h1>
              </div>
              <div className="col-md-2 offset-2">
                {this.Log()}
              </div>
            </div>
          </header>
          <Switch>
            <Route exact path='/' component={Home} />
            <Route exact path='/Sneaker/:shoe' component={Product} />
            <Route exact path='/Login' component={Login} />
          </Switch>
        </div>
      </Router>
    );
  }
}

class SearchBar extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      name: undefined,
      brand: undefined,
      gender: undefined,
      startDate: null,
      endDate: null,
      favorite: false,
    }
  }

  inputChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    })
    this.props.inputaction(e)
  }

  checkboxChange = (e) => {
    this.setState({
      [e.target.name]: e.target.checked
    })
    this.props.checkboxaction(e)
  }

  brandOption() {
    var temp = [<option value={""}>Toutes</option>]
    if (!this.props.option)
      return;
    for (var brand of Object.keys(this.props.option.brand)) {
      temp.push(<option value={brand}>{brand}</option>)
    }
    return temp
  }

  genderOption() {
    var temp = [<option value={""}>Tous</option>]
    if (!this.props.option)
      return;
    for (var gender of Object.keys(this.props.option.gender)) {
      temp.push(<option value={gender}>{gender}</option>)
    }
    return temp
  }

  render() {
    return (
      <div className="Search">
        <h5>Nom de la paire</h5>
        <div className="col-md-6 offset-3 Search">
          <input value={this.state.name} name="name" type='text' placeholder="Recherche" onChange={(e) => this.inputChange(e)}></input>
        </div>
        <h5>Marque</h5>
        <div className="col-md-6 offset-3 Search">
          <select value={this.state.brand} name="brand" onChange={(e) => this.inputChange(e)}>
            {this.brandOption()}
          </select>
        </div>
        <h5>Sexe</h5>
        <div className="col-md-6 offset-3 Search">
          <select value={this.state.gender} name="gender" onChange={(e) => this.inputChange(e)}>
            {this.genderOption()}
          </select>
        </div>
        <h5>Date de sortie</h5>
        <div className="col-md-6 offset-3 Search">
          <div className="row">
            <div className="col-md-6">
              <h6>Après le</h6>
              <input type="date" name="startDate" onChange={(e) => this.inputChange(e)}></input>
            </div>
            <div className="col-md-6">
              <h6>Avant le</h6>
              <input type="date" name="endDate" onChange={(e) => this.inputChange(e)}></input>
            </div>
          </div>
        </div>
        <div className="col-md-6 offset-3">
          <div className="row" style={{ display: "inline-flex" }}>
            <h5 style={{ marginTop: ".5rem" }}>Favoris</h5>
            <Checkbox value={this.state.favorite} icon={<FavoriteBorder />} checkedIcon={<Favorite />} name="favorite" onChange={(e) => this.checkboxChange(e)} />
          </div>
        </div>
      </div>
    )
  }
}

class Sneaker extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      releaseDate: new Date(this.props.shoe.releaseDate),
      favorite: this.props.shoe.favorite
    }
  }

  updateFavorite = (e) => {
    const url = `http://localhost:4000/favorites/${e.target.checked ? "add" : "remove"}/${this.props.shoe.urlKey}`
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      credentials: 'include',
    };
    fetch(url, requestOptions)
    this.props.shoe.favorite = e.target.checked
    this.setState({
      [e.target.name]: e.target.checked
    })
    this.props.updateFavorite()
  }

  render() {
    return (
      <div style={{ height: "inherit" }}>
        <div style={{ textAlign: 'center', paddingBottom: "48px" }}>
          <h1>{this.props.shoe.shoe}</h1>
          <Checkbox checked={this.props.shoe.favorite} icon={<FavoriteBorder />} checkedIcon={<Favorite />} name="favorite" onChange={(e) => this.updateFavorite(e)} />  
          <h6 style={{ marginBottom: '2em' }}>Date de sortie: {this.state.releaseDate.getDate() + "/" + (this.state.releaseDate.getMonth() + 1) + "/" + this.state.releaseDate.getFullYear()}</h6>
          <div className="row">
            <div className="col-md-3 offset-3">
              <p>Prix: {this.props.shoe.retailPrice}$</p>
            </div>
            <div className="col-md-3">
              <p>Prix de revente : {this.props.shoe.market.lowestAsk}$</p>
            </div>
          </div>
          <img src={this.props.shoe.media.thumbUrl} />
        </div>
        <div className="col-md-4 offset-4" style={{ position: "absolute", bottom: "14px" }}>
          <Button href={"/Sneaker/" + this.props.shoe.urlKey} size="lg" block>Voir la paire</Button>
        </div>
      </div>
    )
  }
}

class SneakerList extends React.Component {


  constructor(props) {
    super(props)
    this.updateFavorite = this.updateFavorite.bind(this)
  }

  renderSneaker(shoe) {
    return <Sneaker shoe={shoe} updateFavorite={this.updateFavorite} />;
  }

  updateFavorite() {
    this.forceUpdate()
  }

  howmuch() {
    let tempArray = []
    let filtered = this.props.shoes

    if (!this.props.shoes)
      return;
    filtered = filtered.filter(shoe => {
      if (this.props.options.name && !shoe.shoe.toLowerCase().includes(this.props.options.name.toLowerCase()))
        return false;
      if (this.props.options.brand && shoe.brand !== this.props.options.brand)
        return false;
      if (this.props.options.gender && shoe.gender !== this.props.options.gender)
        return false;
      var startDate = new Date(this.props.options.startDate)
      var endDate = new Date(this.props.options.endDate)
      var releaseDate = new Date(shoe.releaseDate)
      if (releaseDate < startDate || releaseDate > endDate)
        return false;
      if (this.props.options.favorite && !shoe.favorite)
        return false;
      return true;
    })
    console.log("oui")
    for (var obj of filtered) {
      tempArray.push(
        <div className="col-md-6" style={{ margin: "1% 0" }} >
          <ListGroup.Item style={{ height: "100%" }}>{this.renderSneaker(obj)}</ListGroup.Item>
        </div>
      );
    }

    return (tempArray)
  }



  render() {
    return (
      <ListGroup horizontal='md' style={{ flexWrap: "wrap" }}>
        {this.howmuch()}
      </ListGroup>
    )
  }
}


class Home extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      search: "",
      data: {},
      options: {},
      favorites: []
    }
    this.inputhandler = this.inputhandler.bind(this);
    this.checkboxhandler = this.checkboxhandler.bind(this);
  }

  inputhandler(e) {
    var newOpt = this.state.options
    newOpt[e.target.name] = e.target.value
    this.setState({
      options: newOpt
    })
  }

  checkboxhandler(e) {
    var newOpt = this.state.options
    newOpt[e.target.name] = e.target.checked
    this.setState({
      options: newOpt
    })
  }

  componentDidMount() {
    fetch("https://stockx.com/api/browse?productCategory=sneakers&sort=release_date&order=ASC&releaseTime=gte-1584918000&country=FR").then(res => {
      return res.json().then(data => {
        fetch("http://localhost:4000/favorites", { credentials: "include" }).then(res => {
          return res.json().then(favorites => {
            for (var elem of data.Products) {
              elem.favorite = favorites.includes(elem.urlKey)
            }
            this.setState({
              data: data,
              favorites: favorites
            })
          })
        }).catch((e) => {
          this.setState({
            data: data,
          })
        })
      })
    })

  }

  render() {
    return (
      <div>
        <SearchBar inputaction={this.inputhandler} checkboxaction={this.checkboxhandler} option={this.state.data.Facets} />
        <SneakerList options={this.state.options} shoes={this.state.data.Products} favorites={this.state.favorites} />
      </div>
    );
  }
}
