import React from 'react';
import './App.css';
import './Login.css';
import Button from 'react-bootstrap/Button'
import { register } from './serviceWorker';
import { Redirect } from 'react-router-dom';

export default class Login extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      username: "",
      password: ""
    }
  }

  inputChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  register = () => {
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ username: this.state.username, password: this.state.password })
    };
    fetch("http://localhost:4000/register", requestOptions).then((res) => {
      return res.json().then(data => {
        alert(data.message)
      })
    })
  }

  login = () => {
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      credentials: 'include',
      body: JSON.stringify({ username: this.state.username, password: this.state.password }),
    };
    fetch("http://localhost:4000/login", requestOptions).then((res) => {
      if (res.status == 200)
        this.props.history.push("/")
        window.location.reload()
    })
  }

  render() {
    return (
      <div className="Login">
        <div style={{ marginTop: '25%' }}>
          <input value={this.state.name} name="username" type='text' placeholder="Nom d'utilisateur" onChange={(e) => this.inputChange(e)}></input>
        </div>
        <div style={{ marginTop: '1%', marginBottom: "2%" }}>
          <input value={this.state.name} name="password" type='password' placeholder="password" onChange={(e) => this.inputChange(e)}></input>
        </div>
        <div className="row" >
          <div className="col-md-2 offset-3">
            <Button size="sd" block onClick={this.register}>Register</Button>
          </div>
          <div className="col-md-2 offset-2">
            <Button size="sd" block onClick={this.login}>Login</Button>
          </div>
        </div>
      </div>
    );
  }
}