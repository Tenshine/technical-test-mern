# Technical Test MERN

### Temps total: 10h30

 détail:
* Sujet principal (calendrier + recherche par nom) -> 3h
* tri supplémentaire (marque, sexe, date de sortie) -> 2h  
* page de sneaker -> 1h30  
* mise en place de l'API et de la Database (gestion d'utilisateur et favoris ->  2h  
* login/register front -> 30min  
* gestion des favoris + tri par favoris -> 1h30


## Website:
    
prérequis: npm
    
pour lancer le site web il faut se rendre dans le dossier "my-app", les commandes `npm install` puis `npm start`  
le site web peut être utilisé en standalone cependant les features liés l'API ne seront pas accessible (gestion d'utilisateur et sauvegarde favoris) et l'affichage du calendrier sera légèrement plus long car du timeout de l'API (recupération des favoris)

## API:
    
prérequis: npm, node, mongodb

pour lancer l'api il faut avoir une base de donnée mongodb en local, se rendre dans le dossier "api" et utiliser les commande `npm install` puis `node index.js`
si l'api se lance sans aucun problème elle affichera "App listening on port 4000"